'use strict';
angular.module('myApp').controller('CommentController', function CommentController ($scope, $http, $q, $auth) {
    
    $scope.newComment = function(contributionID, commentBody) {
        if (commentBody == ""){
            alert("Comment can't be empty");
        } else {
            console.log("Comment post: contributionID: " + contributionID + ", body: " + commentBody);
            var req = {
                method: 'POST',
                url: 'https://asw-software33-alr67.c9users.io/contribution/comment/',
                headers: {
                    'content-type': 'application/json',
                    'authorization': 'Token token=6a20fb71b3364946af5048a799b62893',
                    'Accept': 'application/json'
                },
                data: {
                    contribution_id: contributionID,
                    body: commentBody
                }
            };
              
            $http(req).success(function(data, status, headers, config, scope) {
                console.log("Comment created");
                console.log(data);
            });
        }
    };
    
    $scope.deleteComment = function(commentID) {
        console.log("Deleting comment, ID: " + commentID);
        var req = {
            method: 'PUT',
            url: 'https://asw-software33-alr67.c9users.io/comment/delete/',
            headers: {
                'content-type': 'application/json',
                'authorization': 'Token token=6a20fb71b3364946af5048a799b62893',
                'Accept': 'application/json'
            },
            data: {
                id: commentID
            }
        };
          
        $http(req).success(function(data, status, headers, config) {
            console.log("Comment deleted");
            console.log(data);
        });
    };
    
    $scope.upvoteComment = function(commentID) {
        console.log("Upvoting comment, ID: " + commentID);
        var req = {
            method: 'PUT',
            url: 'https://asw-software33-alr67.c9users.io/comment/upvote/',
            headers: {
                'content-type': 'application/json',
                'authorization': 'Token token=6a20fb71b3364946af5048a799b62893',
                'Accept': 'application/json'
            },
            data: {
                id: commentID
            }
        };
          
        $http(req).success(function(data, status, headers, config) {
            console.log("Comment Upvoted");
            console.log(data);
        });
    };
    
    
});