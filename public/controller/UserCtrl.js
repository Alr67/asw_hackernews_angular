'use strict';


angular.module('myApp').controller('UserController', function UserController ($scope, $http, $q, $auth, $routeParams, $rootScope, $location) {
    
     var userID = getIdGuai();
     
    function getIdGuai() { 
        var url =  $location.absUrl().split("/");
        return url[(url.length)-1];
    }
     // GET CONTRIBUTIONS
    $http.get('https://asw-software33-alr67.c9users.io/users/'+userID+'', { 
      headers: {
            'Accept': 'application/json',
            'Authorization': 'Token token=6a20fb71b3364946af5048a799b62893'
        }
    })
    .success(function(data, status, headers, config) {
        console.log("Get user");
        console.log(data);
        $scope.user = data;
      });
      
       $scope.updateUser = function(about, mail) {
         var req = {
                method: 'PUT',
                url: 'https://asw-software33-alr67.c9users.io/user/edit',
                headers: {
                    'content-type': 'application/json',
                    'authorization':'Token token=6a20fb71b3364946af5048a799b62893',
                    'Accept': 'application/json'
                },
    		    data: {
    				id: userID,
    				mail: mail,
    				about: about
    		    }
            };
          
            $http(req).success(function(data, status, headers, config) {
                return "Success";
            });
     };
     
     $scope.reloadRoute = function() {
        $route.reload();
     }
});
