'use strict';
angular.module('myApp').controller('ReplyController', function ReplyController ($scope, $http, $q, $auth, $location, $routeParams, $rootScope) {
    
    $scope.newReply = function(replyBody) {
        if (replyBody == ""){
            alert("Reply can't be empty");
        } else {
            console.log($rootScope.absUrl)
            var commentID = getIdGuai();
            console.log("Reply post: commentID: " + commentID + ", body: " + replyBody);
            var req = {
                method: 'POST',
                url: 'https://asw-software33-alr67.c9users.io/contribution/comment/reply/',
                headers: {
                    'content-type': 'application/json',
                    'authorization': 'Token token=6a20fb71b3364946af5048a799b62893',
                    'Accept': 'application/json'
                },
                data: {
                    comment_id: commentID,
                    body: replyBody
                }
            };
              
            $http(req).success(function(data, status, headers, config) {
                console.log("Reply created");
                console.log(data);
            });
        }
    };
    
    $scope.deleteReply = function(replyID) {
        console.log("Deleting reply, ID: " + replyID);
        var req = {
            method: 'PUT',
            url: 'https://asw-software33-alr67.c9users.io/reply/delete/',
            headers: {
                'content-type': 'application/json',
                'authorization': 'Token token=6a20fb71b3364946af5048a799b62893',
                'Accept': 'application/json'
            },
            data: {
                id: replyID
            }
        };
          
        $http(req).success(function(data, status, headers, config) {
            console.log("Reply deleted");
            console.log(data);
        });
    };
    
    $scope.upvoteReply = function(replyID) {
        console.log("Upvoting reply, ID: " + replyID);
        var req = {
            method: 'PUT',
            url: 'https://asw-software33-alr67.c9users.io/reply/upvote/',
            headers: {
                'content-type': 'application/json',
                'authorization': 'Token token=6a20fb71b3364946af5048a799b62893',
                'Accept': 'application/json'
            },
            data: {
                id: replyID
            }
        };
          
        $http(req).success(function(data, status, headers, config) {
            console.log("Reply deleted");
            console.log(data);
        });
    };
    
    function getIdGuai() { 
        var url =  $location.absUrl().split("/");
        return url[(url.length)-1];
    }
});