'use strict';


angular.module('myApp').controller('ContributionController', function ContributionController ($scope, $http, $q, $auth, $routeParams, $rootScope, $location) {

    $scope.allContributions = [];
    
    // GET CONTRIBUTIONS
    $http.get('https://asw-software33-alr67.c9users.io/contributions', { 
      headers: {
            'Accept': 'application/json',
            'Authorization': 'Token token=6a20fb71b3364946af5048a799b62893'
        }
    })
    .success(function(data, status, headers, config) {
        console.log("Get contributions");
        console.log(data);
        $scope.contributions = data;
        $scope.allContributions.push(data);
      });
      
    // GET ASKS
    $http.get('https://asw-software33-alr67.c9users.io/ask', { 
      headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Token token=6a20fb71b3364946af5048a799b62893'
        }
    })
    .success(function(data, status, headers, config) {
        console.log("Get Ask");
        console.log(data);
        $scope.asks = data;
        $scope.allContributions.push(data);
      });
      
    // GET COMMENTS
    $http.get('https://asw-software33-alr67.c9users.io/comments', { 
      headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Token token=6a20fb71b3364946af5048a799b62893'
        }
    })
    .success(function(data, status, headers, config) {
        console.log("Get Comments");
        console.log(data);
        $scope.comments = data;
      });
      
      
      // GET Users
    $http.get('https://asw-software33-alr67.c9users.io/users', { 
      headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Token token=6a20fb71b3364946af5048a799b62893'
        }
    })
    .success(function(data, status, headers, config) {
        console.log("Get Users");
        console.log(data);
        $scope.users = data;
      });
      
      
    function findCherries(fruit) { 
        return $scope.allContributions.id == $routeParams.id;
    }
    

    
     $scope.getContribution = function() {
         return $scope.allContributions.find(findCherries);
         
     };
     
     
     $scope.getCreatorId = function (post){
         var   url = post.userURL.split("/");
         return url[(url.length)-1];
     };
     
     $scope.getContributionId = function() {
         return getIdGuai();
    };
     
     function getIdGuai() { 
        var url =  $location.absUrl().split("/");
        return url[(url.length)-1];
    }
     
    function findUser(fruit) { 
        return $scope.users.id == $routeParams.id;
    }
    
     $scope.getUser = function() {
         console.log("MY User:");
         console.log( $scope.users.find(findUser));
         return $scope.users.find(findUser);
     };
     
     $scope.createComment = function(text, id) {
         var req = {
                method: 'POST',
                url: 'https://asw-software33-alr67.c9users.io/contribution/comment',
                headers: {
                    'content-type': 'application/json',
                    'authorization':'Token token=6a20fb71b3364946af5048a799b62893',
                    'Accept': 'application/json'
                },
    		    data: {
    				body: text,
    				contribution_id: id
    		    }
            };
          
            $http(req).success(function(data, status, headers, config) {
            return "Success";
            });
     };
     
    //FUNCIONS DEL CONTROLLER
    $scope.upvoteComment = function(contributionID) {
        console.log("Votando");
        var req = {
            method: 'PUT',
            url: 'https://asw-software33-alr67.c9users.io/contribution/upvote/',
            headers: {
                'content-type': 'application/json',
                'authorization': 'Token token=6a20fb71b3364946af5048a799b62893',
                'Accept': 'application/json'
            },
            data: {
                id: contributionID
            }
        };
          
        $http(req).success(function(data, status, headers, config) {
            console.log("Contribution Upvoted");
            console.log(data);
        });
    };
    
      
      $scope.createContribution = function(title,URL,textBody) {
        console.log("Lets POSTTT; " + "title: " + title + "URL: " + URL + "text: " + textBody);
            var req = {
                method: 'POST',
                url: 'https://asw-software33-alr67.c9users.io/contributions/publish',
                headers: {
                    'content-type': 'application/json',
                    'authorization': 'Token token=6a20fb71b3364946af5048a799b62893',
                    'Accept': 'application/json'
                },
    		    data: {
    				title: title,
    				url: URL,
    				text: textBody
    		    }
            };
          
            $http(req).success(function(data, status, headers, config) {
                console.log("Success del post");
                console.log(data);
                return "Success";
            });
      };
      
     
    $scope.deleteContribution = function(contributionID) {
        console.log("Deleting contribution, ID: " + contributionID);
        var req = {
            method: 'PUT',
            url: 'https://asw-software33-alr67.c9users.io/contribution/delete/',
            headers: {
                'content-type': 'application/json',
                'authorization': 'Token token=6a20fb71b3364946af5048a799b62893',
                'Accept': 'application/json'
            },
		    data: {
				id: contributionID
		    }
        };
      
        $http(req).success(function(data, status, headers, config) {
            console.log("Contribution deleted");
            console.log(data);
            return "Success";
        });
    };
      
      //NO BORRAR!!!!!!!!!!!!!!!!!!
      $scope.newComment = function(contributionID, commentBody) {
        if (commentBody == ""){
            alert("Comment can't be empty");
        } else {
            console.log("Comment post: contributionID: " + contributionID + ", body: " + commentBody);
            var req = {
                method: 'POST',
                url: 'https://asw-software33-alr67.c9users.io/contribution/comment/',
                headers: {
                    'content-type': 'application/json',
                    'authorization': 'Token token=6a20fb71b3364946af5048a799b62893',
                    'Accept': 'application/json'
                },
                data: {
                    contribution_id: contributionID,
                    body: commentBody
                }
            };
              
            $http(req).success(function(data, status, headers, config) {
                console.log("Comment created");
                console.log(data);
            });
        }
    };
});
