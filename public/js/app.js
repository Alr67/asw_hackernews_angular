var myApp = angular.module('myApp', ['ngRoute','satellizer'])
 .config(function( $routeProvider, $authProvider) {
    
    $routeProvider.when('/contributions/:id', {
        templateUrl: 'showContribution.html',    
        controller: 'ContributionController'
    });
    $routeProvider.when('/reply/:idR', {
        templateUrl: 'newReply.html',    
        controller: 'ReplyController'
    });
     //Redirecció TODO:Refer si es necessari, aixo es vell
      $routeProvider.otherwise('/home');
        $routeProvider.when('', {
            templateUrl: '/index.html',
            controller: 'ContributionController'
        })
       .when('/asks', {
                controller: function(){
                    window.location.href = '/ask.html';
            }
        })
  });
  
   myApp.filter('shortURL', function () {
      return function (text) {
          var matches, 
            output = "",
            urls = /\w+:\/\/([\w|\.]+)/;
          matches = urls.exec(text);
          
          if (matches !== null) {output = matches[1];}
          
          return output;
      };
  });
  
  
  
  